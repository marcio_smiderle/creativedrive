# coding=utf8
import sys
from PySide2.QtWidgets import (QApplication, QLabel, QPushButton,
                               QVBoxLayout, QHBoxLayout, QWidget, QLineEdit,
                               QFileDialog, QStyle)
from PySide2.QtCore import Signal, Slot, Qt, QFile, QObject, QAbstractItemModel
from PySide2.QtGui import QStandardItem, QStandardItemModel
from PySide2.QtUiTools import QUiLoader
import requests
import os

class LoadedUi(QObject):
    Centered = 0

    def __init__(self, filename, position=Centered):
        QObject.__init__(self)
        f = QFile(filename)
        f.open(QFile.ReadOnly)
        loader = QUiLoader()
        self.ui = loader.load(f)
        f.close()
        if position == LoadedUi.Centered:
            self.ui.setGeometry(
                QStyle.alignedRect(
                    Qt.LeftToRight, Qt.AlignCenter,
                    self.ui.size(), QApplication.desktop().availableGeometry()))

    def __getattr__(self, name):
        # if self.__dict__.get(name) is not None:
        # only necessary if __getattribute__ is used
        #     return self.__dict__[name]
        # else:
        return getattr(self.ui, name)


class LoginDialog(LoadedUi):
    def __init__(self):
        LoadedUi.__init__(self, "login.ui")

class CDClientView(LoadedUi):
    loginStarted = Signal(str, str)
    uploadFileClicked = Signal(str)

    def __init__(self):
        LoadedUi.__init__(self, "main.ui")

        model = QStandardItemModel(0, 2)
        model.setHorizontalHeaderLabels(['file', 'status', 'upload'])
        self.uploadsLst.setModel(model)

        self.login = LoginDialog()
        self.login.cancelBtn.released.connect(self.login.hide)
        self.login.loginBtn.released.connect(self.loginAction)

        # Connecting the signal
        self.uploadDlg.clicked.connect(self.uploadDialog)
        self.loginDlg.clicked.connect(self.loginDialog)

    @Slot()
    def addUploadToList(self, filename):
        items = self.uploadsLst.model().findItems(filename)
        if len(items) > 0:
            item = items[0]
            item.setText(filename)
        else:
            item = QStandardItem(filename)
            item2 = QStandardItem('row added')
            self.uploadsLst.model().appendRow([item, item2])
            btn = QPushButton("start", self.uploadsLst)
            index = self.uploadsLst.model().rowCount()
            self.uploadsLst.setIndexWidget(self.uploadsLst.model().index(index - 1, 2), btn)
            self.uploadsLst.resizeColumnToContents(0)
            emitOnClick = lambda : self.uploadFileClicked.emit(filename)
            btn.clicked.connect(emitOnClick)
            self.updateUploadStatus(filename, 'added')

    @Slot()
    def updateUploadStatus(self, filename, status):
        items = self.uploadsLst.model().findItems(filename)
        if len(items) > 0:
            item  = items[0]
            index = self.uploadsLst.model().indexFromItem(item)
            row   = index.row()
            item  = self.uploadsLst.model().itemFromIndex(index.sibling(row, 1))
            item.setText(status)

    @Slot()
    def loginDialog(self):
        self.login.show()

    @Slot()
    def uploadDialog(self):
        filename = QFileDialog.getOpenFileName(self.ui, "Choose a file for upload")
        self.addUploadToList(filename[0])

    @Slot()
    def loginAction(self):
        email    = self.login.email.text()
        password = self.login.password.text()
        self.loginStarted.emit(email, password)

class CdClient(QObject):
    responseReceived  = Signal(str)
    loginDone         = Signal()
    fileSendStarted   = Signal(str)
    fileSendCompleted = Signal(str)
    fileSendFailed    = Signal(str)

    def __init__(self):
        QObject.__init__(self)
        self.token = ''

    @Slot()
    def login(self, email, password):
        # rest here
        url = os.getenv('CD_API', 'http://localhost:5000') + '/login'
        try:
            data = {"email": email, "password": password}
            response = requests.post(url, json=data)
            if response.status_code == 200:
                json = response.json()
                self.token = json['token']
                self.responseReceived.emit('ok! :-)')
                self.loginDone.emit()
            elif response.status_code == 403:
                json = response.json()
                self.responseReceived.emit(json['error'])
            else:
                self.responseReceived.emit('unknown error :-/')
        except:
            self.responseReceived.emit('unknown error :-/')

    @Slot()
    def upload(self, filename):
        # rest here
        url = os.getenv('CD_API', 'http://localhost:5000') + '/upload'
        try:
            headers = {'Authorization': self.token}
            files = {'file': open(filename, 'rb')}
            self.fileSendStarted.emit(filename)
            response = requests.post(url, headers=headers, files=files)
            if response.status_code == 200:
                self.responseReceived.emit(
                    'file named "' + os.path.basename(filename) +
                    '" was uploaded')
                self.fileSendCompleted.emit(filename)
            elif response.status_code == 403:
                json = response.json()
                self.responseReceived.emit(json['error'])
                self.fileSendFailed.emit(filename)
            else:
                self.responseReceived.emit('unknown error uploading file :-/')
                self.fileSendFailed.emit(filename)
        except:
            self.responseReceived.emit('unknown error uploading file :-/')
            self.fileSendFailed.emit(filename)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    cd = CdClient()

    mwin = CDClientView()
    mwin.resize(1024, 768)
    mwin.show()

    mwin.loginStarted.connect(cd.login)
    mwin.uploadFileClicked.connect(cd.upload)
    cd.fileSendStarted.connect(lambda filename : mwin.updateUploadStatus(filename, 'sending'))
    cd.fileSendFailed.connect(lambda filename : mwin.updateUploadStatus(filename, 'fail'))
    cd.fileSendCompleted.connect(lambda filename : mwin.updateUploadStatus(filename, 'completed'))
    cd.responseReceived.connect(mwin.statusbar.showMessage)
    cd.loginDone.connect(mwin.login.hide)
    
    sys.exit(app.exec_())
