(defpackage :cd-client
  (:use :common-lisp :eql)
  (:export
   #:*cd-client*
   #:start))

(in-package :cd-client)

(defun load-ui (filename)
  (let* ((w (qload-ui filename))
	 (ar (|alignedRect.QStyle| |Qt.LeftToRight| |Qt.AlignCenter|
				   (|size| w)
				   (|availableGeometry| (|desktop.QApplication|)))))
    (|setGeometry| w ar)
    w))

(defvar *cd-client* (load-ui "main.ui"))
(defvar *login*     (load-ui "login.ui"))

(defvar login-btn   (qfind-child *cd-client* "loginDlg"))
(defvar upload-btn  (qfind-child *cd-client* "uploadDlg"))
(defvar uploads-lst (qfind-child *cd-client* "uploadsLst"))
(defvar cancel-btn  (qfind-child *login*     "cancelBtn"))

(defun initialize ()
  (let ((model (qnew "QStandardItemModel")))
    (|setModel| uploads-lst model)))

(defun uploadDialog ()
  (let ((filename (|getOpenFileName.QFileDialog| *cd-client* "Choose a file for upload")))
    (if filename
        (addUploadToList filename))))

(defun addUploadToList(filename)
  (let* ((model (|model| uploads-lst))
         (items (! "findItems" ("QStandardItemModel" model) filename)))
    (if items
        (qfun (car items) "setText" filename)
        (progn
          (let* ((item  (qnew "QStandardItem(QString)" filename))
                 (item2 (qnew "QStandardItem(QString)" "row added"))
                 (item3 (qnew "QStandardItem(QString)" "controls"))
                 (btn   (qnew "QPushButton(QString, QWidget*)" "start" uploads-lst))
                 (row   (list item item2 item3)))
            (! "appendRow" ("QStandardItemModel" model) row)
            (|setIndexWidget| uploads-lst
                              (|index| model (- (|rowCount| model) 1) 2)
                              btn)
            (|resizeColumnToContents| uploads-lst 0)
            (qconnect btn "clicked()" (lambda () (format t "~A ~A~%" "started " filename)))
            ))
        )))

(defun start ()
  (initialize)
  (qconnect login-btn  "clicked()"  (lambda () (|show| *login*)))
  (qconnect cancel-btn     "released()" (lambda () (|hide| *login*)))
  (qconnect upload-btn "clicked()"  'uploadDialog)
  (x:do-with *cd-client* |show| |raise|))

(start)
