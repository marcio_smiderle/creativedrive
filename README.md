# Foreword

This program was made with python 3 and pySide qt binding library. They should be already installed before running the commands bellow.

# Installantion

Download or clone this repository in any directory.
`
$ git clone https://bitbucket.org/marcio_smiderle/creativedrive.git
$ cd creativedrive
`

# Server

`
$ cd cd-api
$ pip install -r requirements.txt
$ FLASK_APP=cd-api.py flask run
`

# Client

`
$ cd cd-client
$ python cd-client.py
`

# Usage

You must sign in before upload files. Use test@test.com as email and 123qwe as password.
