from flask import Flask
from flask import jsonify, abort
from flask import request
from werkzeug.utils import secure_filename
import os
from functools import wraps
import time
import base64

app = Flask(__name__)

@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404

@app.errorhandler(403)
def handle_invalid_usage(e):
    return jsonify(error=str(e)), 403

@app.route('/')
def hello_world():
    return {'message': 'Welcome!'}

users = {'test@test.com': '123qwe'}
logged_in = {}

def encodeToken(email, aTime):
    token = email + '|' + str(aTime)
    return base64.b64encode(token.encode()).decode()

def decodeToken(token):
    return base64.b64decode(token).decode().split('|')

@app.route('/login', methods=['POST'])
def login():
    json = request.get_json()
    if json is None or json.get("email") is None or json.get("password") is None:
        abort(403, description='email and password are required')
    email    = json.get('email')
    password = json.get('password')
    if users.get(email) == password:
        t = time.time()
        token = email + '|' + str(t)
        token = encodeToken(email, t)
        setLoggedIn(email, t)
        return {'token': token}, 200
    else:
        abort(403, description='denied')

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not isLoggedIn():
            abort(403, description='denied')
        return f(*args, **kwargs)
    return decorated_function

def setLoggedIn(email, aTime):
    logged_in[email] = aTime

def setLoggedOut(email):
    if logged_in.get(email) is not None:
        del logged_in[email]

def isLoggedIn():
    emailTime = request.headers.get('Authorization')
    if emailTime is not None:
        emailTime = decodeToken(emailTime)
        if len(emailTime) == 2:
            email = emailTime[0]
            time = float(emailTime[1])
            if logged_in.get(email) == time:
                return True
    return False

@app.route('/me', methods=['GET'])
@login_required
def me():
    email = request.headers.get('Authorization')
    if email is not None:
        email = decodeToken(email)[0]
    return {"email": email}

@app.route('/logout', methods=['POST'])
@login_required
def logout():
    email = request.headers.get('Authorization')
    if email is not None:
        email = decodeToken(email)[0]
    setLoggedOut(email)
    return {}

@app.route('/upload', methods=['POST'])
@login_required
def upload_file():
    if 'file' not in request.files:
        abort(403, description='file is empty')
    f = request.files['file']
    if f.filename == '':
        abort(403, description='file is empty')
    filename = secure_filename(f.filename)
    f.save(os.path.join('./uploads/', filename))
    return {}
